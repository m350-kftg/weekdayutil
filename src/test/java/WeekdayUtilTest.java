import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Tests for class WeekdayUtil")
public class WeekdayUtilTest {

    @Nested
    @DisplayName("Tests for method get")
    class GetWeekday {

        @Test
        public void dateOfStartOfSchoolIsOnMonday() {
            // given
            LocalDate dateOfStartOfSchool = LocalDate.of(2023, 8, 14);
            // when
            Weekday weekdayOfStartOfSchool = WeekdayUtil.getWeekday(dateOfStartOfSchool);
            //then
            assertEquals(Weekday.MONDAY, weekdayOfStartOfSchool);
        }
    }

    @Nested
    @DisplayName("Tests for method isWeekend")
    class IsWeekend {

        @Test
        public void dateOfStartOfSchoolIsNotOnWeekend() {
            // given
            LocalDate dateOfStartOfSchool = LocalDate.of(2023, 8, 14);
            // when
            boolean isOfStartOfSchoolWeekend = WeekdayUtil.isWeekend(dateOfStartOfSchool);
            //then
            assertFalse(isOfStartOfSchoolWeekend);
        }

        @Test
        public void firstDateIn21CenturyIsOnWeekend() {
            // given
            LocalDate firstDateIn21Century = LocalDate.of(2000, 1, 1);
            // when
            boolean isFirstDateIn21Century = WeekdayUtil.isWeekend(firstDateIn21Century);
            //then
            assertTrue(isFirstDateIn21Century);
        }
    }
}
