import java.time.LocalDate;

public class WeekdayUtil {

    public static Weekday getWeekday(LocalDate date) {
        int d = date.getDayOfMonth();
        int m = date.getMonthValue();
        int y = date.getYear();

        if (m <= 2) {
            m += 10;
            y -= 1;
        } else {
            m -= 2;
        }
        int a = (26 * m - 2) / 10;
        int u = y / 100;
        int v = y % 100;
        int w = u / 4;
        int x = v / 4;
        int h = (a + d + -2 * u + v + w + x) % 7;
        if (h < 0) {
            h += 7;
        }
        return Weekday.get(h);
    }

    public static boolean isWeekend(LocalDate date) {
        Weekday weekday = getWeekday(date);
        return weekday.isWeekend();
    }

}
