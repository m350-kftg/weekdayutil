public enum Weekday {
    MONDAY(1, false),
    TUESDAY(2, false),
    WEDNESDAY(3, false),
    THURSDAY(4, false),
    FRIDAY(5, false),
    SATURDAY(6, true),
    SUNDAY(0, true);

    private int value;
    private boolean isWeekend;

    Weekday(int value, boolean isWeekend) {
        this.value = value;
        this.isWeekend = isWeekend;
    }

    public boolean isWeekend(){
        return isWeekend;
    }

    public static Weekday get(int value){
        for (Weekday weekday : Weekday.values()) {
            if (weekday.value == value){
                return weekday;
            }
        }
        return null;
    }

}
